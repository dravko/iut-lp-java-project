# Design Patterns Demos

Lien vers le sujet : https://www.mickael-martin-nevot.com/univ-amu/iut/lp-web/algorithmique-et-uml/s03-projet.pdf

- TD 4-2 : https://www.mickael-martin-nevot.com/univ-amu/iut/lp-web/algorithmique-et-uml/s03-td4-2-java-cas-pratique.pdf 
- TD 5 : https://www.mickael-martin-nevot.com/univ-amu/iut/lp-web/algorithmique-et-uml/s03-td5-algorithmique-avancee.pdf
- TD 6 : https://www.mickael-martin-nevot.com/univ-amu/iut/lp-web/algorithmique-et-uml/s03-td6-uml.pdf

# Lancer les tests

Note: **Pas besoin d'installer `gradle`** pour lancer les tests.

- Sur Linux / Mac : `./gradlew test`
- Sur Windows : `./gradlew.bat test`
